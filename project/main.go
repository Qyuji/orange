package main

import (
	"gitee.com/zhucheer/orange/app"
	"gitee.com/zhucheer/orange/cfg"
	"gitee.com/zhucheer/orange/project/http"
)

func main() {

	cfg.SetIntFlag("opennum", 1, "open num ")

	router := &http.Route{}
	app.AppStart(router)

}
