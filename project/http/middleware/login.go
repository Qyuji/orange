package middleware

import (
	"fmt"
	"gitee.com/zhucheer/orange/app"
)

type Login struct {
}

func NewLogin() *Login {
	return &Login{}
}

// Func implements Middleware interface.
func (w Login) Func() app.MiddlewareFunc {
	return func(next app.HandlerFunc) app.HandlerFunc {
		return func(c *app.Context) error {

			fmt.Println("login middleware")

			return next(c)
		}
	}
}
