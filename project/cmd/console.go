package main

import (
	"gitee.com/zhucheer/orange/app"
	"gitee.com/zhucheer/orange/project/http"
)

func main() {
	router := &http.Route{}
	app.ConsoleStart(router)
}
