package view

import (
	"gitee.com/zhucheer/orange/cfg"
	"github.com/stretchr/testify/assert"
	"os"
	"path/filepath"
	"testing"
)

func startCfg() {
	cfg.ParseParams()
	cfg.StartConfig()
}

// go test view_test.go view.go parse.go -v -args --config=../project/config/config.toml
func TestParseViewName(t *testing.T) {
	startCfg()
	path1 := parseViewName("index")

	workDir, _ := os.Getwd()
	tplPath := filepath.Join(workDir, "/storage/views/index.tpl")
	assert.Equal(t, path1, tplPath)

	path2 := parseViewName("home.index")

	tplPath = filepath.Join(workDir, "/storage/views/home/index.tpl")
	assert.Equal(t, path2, tplPath)

}

func TestViewAssigns(t *testing.T) {
	startCfg()

	showData := map[string]interface{}{
		"Title": "Orange view",
	}
	showData2 := struct {
		Title2 string
	}{
		Title2: "Orange view2",
	}

	tmpl := ContextTmpl("home.index").Assigns(showData).Assigns(showData2).Assign("Content", "hello")
	if len(tmpl.ShowData) != 3 {
		t.Error("Assigns have an error #1")
	}

	workDir, _ := os.Getwd()
	tplPath := filepath.Join(workDir, "/storage/views/home/index.tpl")

	assert.Equal(t, tmpl.ViewPath, tplPath)

	_, _ = tmpl.Render()
}
