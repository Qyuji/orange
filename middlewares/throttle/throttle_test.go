package throttle

import (
	"context"
	"gitee.com/zhucheer/orange/app"
	orangerequest "gitee.com/zhucheer/orange/request"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestNewThrottle(t *testing.T) {
	throttle := NewThrottle(5, 3*time.Second, true)
	httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("REMOTE_ADDR") != "" {
			r.RemoteAddr = r.Header.Get("REMOTE_ADDR")
		}
		ctx := app.NewCtx(context.Background(), w, r)
		ctx.OrangeInput = orangerequest.NewInput(r, 20480)

		middleWare := throttle.Func()
		action := middleWare(func(ctx *app.Context) error {
			return nil
		})
		action(ctx)

	}))
	defer httpServer.Close()

	var defaultClient = &http.Client{}
	req, _ := http.NewRequest("GET", httpServer.URL, nil)
	defaultClient.Do(req)

	req.Header.Add("REMOTE_ADDR", "192.168.1.100")
	defaultClient.Do(req)

	if len(throttle.requestMaps) != 2 {
		t.Error("NewThrottle have error #1")
	}

	defaultClient.Do(req)
	// "192.168.1.100" IP请求限速对象
	if throttle.requestMaps["f18OqT"].BucketHandler.Available() != 3 {
		t.Error("NewThrottle have error #2")
	}

	time.Sleep(time.Second)
	defaultClient.Do(req)
	defaultClient.Do(req)
	defaultClient.Do(req)
	defaultClient.Do(req)
	defaultClient.Do(req)
	if throttle.requestMaps["f18OqT"].BucketHandler.Available() > 0 {
		t.Error("NewThrottle have error #3")
	}

	time.Sleep(500 * time.Millisecond)
	if throttle.requestMaps["f18OqT"].BucketHandler.Available() < 2 {
		t.Error("NewThrottle have error #4")
	}
	time.Sleep(9 * time.Second)

	if len(throttle.requestMaps) > 0 {
		t.Error("NewThrottle have error #5")
	}
}

func TestNewThrottleConcurency(t *testing.T) {
	throttle := NewThrottle(2, time.Minute, true)
	httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := app.NewCtx(context.Background(), w, r)
		ctx.OrangeInput = orangerequest.NewInput(r, 20480)

		middleWare := throttle.Func()
		action := middleWare(func(ctx *app.Context) error {
			return nil
		})
		action(ctx)
	}))

	defer httpServer.Close()

	var defaultClient = &http.Client{}
	req, _ := http.NewRequest("GET", httpServer.URL, nil)
	defaultClient.Do(req)
	defaultClient.Do(req)
	defaultClient.Do(req)

	time.Sleep(10 * time.Second)
	resp, err := defaultClient.Do(req)

	if err != nil {
		t.Error("ThrottleConcurency have an error #1")
	}

	if resp.StatusCode != 429 {
		t.Error("ThrottleConcurency break have an error #1")
	}
}
