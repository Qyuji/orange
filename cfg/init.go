package cfg

import (
	"errors"
	"fmt"
	"gitee.com/zhucheer/cfg"
	"gitee.com/zhucheer/orange/utils"
)

var Config = cfg.New("")
var ConfigDef = cfg.New("")

func init() {
	NewFlagParam()
	// 注册配置文件参数
	SetStringFlag("config", "./config/config.toml", "config file path")
	SetIntFlag("port", 0, "http server default port")
	SetIntFlag("grpc", 0, "grpc server default port")
	SetStringFlag("jobRun", "orange", "console exec job name")
}

func ReloadConfig() {
	configPath := GetStringFlag("config")
	StartConfigWithPath(configPath)
}

// 启动配置服务
func StartConfig() {
	configPath := GetStringFlag("config")
	StartConfigWithPath(configPath)
}

// StartConfigWithPath 通过配置文件路径加载配置
func StartConfigWithPath(configPath string) {
	if isFile, _ := utils.FileExists(configPath); isFile == false {
		fmt.Println(fmt.Sprintf("\033[0;33m%v \033[0m", "[WARNING] "+configPath+" not found, please check the config path"))
		Config = cfg.NewTomlData(defaultConfig)
	} else {
		fmt.Println(fmt.Sprintf("\033[0;33m%v \033[0m", "[ORANGE] loaded config with "+configPath))
		Config = cfg.New(configPath)
	}

	if Config == nil {
		panic("config init failed")
	}

	// 静态配置则不开启热加载
	if Config.GetBool("app.configStatic") == false {
		Config.StartDynamic()
	}

	// 加载默认配置对象
	ConfigDef = cfg.NewTomlData(defaultConfig)
}

// UnmarshalKey 将配置绑定到结构体
func UnmarshalKey(key string, rawVal interface{}) error {
	if Config.Exists(key) {
		return Config.UnmarshalKey(key, rawVal)
	}

	return errors.New("config key not found")
}

// GetString 获取字符型配置
func GetString(key string, defaultVal string) string {
	if Config.Exists(key) {
		return Config.GetString(key)
	}
	return defaultVal
}

// GetInt 获取int类型配置
func GetInt(key string, defaultVal int) int {
	if Config.Exists(key) {
		return Config.GetInt(key)
	}
	return defaultVal
}

// GetInt64 获取int类型配置
func GetInt64(key string, defaultVal int64) int64 {
	if Config.Exists(key) {
		return Config.GetInt64(key)
	}
	return defaultVal
}

// GetBool 获取bool类型配置
func GetBool(key string, defaultVal bool) bool {
	if Config.Exists(key) {
		return Config.GetBool(key)
	}
	return defaultVal
}

// GetSliceString 获取slice
func GetSliceString(key string, defaultVal []string) []string {
	if Config.Exists(key) {
		return Config.GetSliceString(key)
	}
	return defaultVal
}
