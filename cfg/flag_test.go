package cfg

// go test flag_test.go flag.go init.go default.go -v -args --config=./config.toml

import (
	"io"
	"os"
	"strings"
	"testing"
)

func TestStartConfig(t *testing.T) {

	writeFile("config.toml", []byte(defaultConfig), 0600)
	defer func() {
		os.Remove("config.toml")
	}()
	StartConfig()

	appName := Config.GetString("app.name")
	if appName != "orange" {
		t.Error("StartConfig error #1")
	}

	defaultConfig = strings.Replace(defaultConfig, `name = "orange"`, `name = "orange-change"`, 1)
	writeFile("config.toml", []byte(defaultConfig), 0600)

	ReloadConfig()

	appName = Config.GetString("app.name")
	if appName != "orange-change" {
		t.Error("StartConfig error #1")
	}
}

func writeFile(filename string, data []byte, perm os.FileMode) error {
	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, perm)
	if err != nil {
		return err
	}
	n, err := f.Write(data)
	if err == nil && n < len(data) {
		err = io.ErrShortWrite
	}
	if err1 := f.Close(); err == nil {
		err = err1
	}
	return err
}
