package cfg

//默认配置,配置文件不存在时会加载默认配置

var defaultConfig = `[app]
    name = "orange"
    key = "orangeDefaultConfig"
    httpAddr = "0.0.0.0"
    httpPort = 8088
	grpcAddr = "0.0.0.0"
	grpcPort = 8089
    maxBody = 2097152
    csrfVerify = false
    pprofOpen = false
	accessLog = true
    maxWaitSecond = 120
	pidfile = ""
    viewPath = "./storage/views"
    [app.logger]
        level = "INFO"
        type = "text"
        path = ""
        syncInterval = 500
		maxCap = 5000
    [app.session]
        isOpen = false
        timeout = 1800
    [app.upload]
        storage = "./storage/allimg"
        maxSize = 2097152
        ext = ["jpg","jpeg","png","gif"]
[mailer]

[database]
    initCap = 2
    maxCap = 5
    idleTimeout = 5
    debug = true
    [database.mysql]

    [database.redis]
`
