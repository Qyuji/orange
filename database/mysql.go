package database

import (
	"errors"
	"fmt"
	"gitee.com/zhucheer/orange/cfg"
	"gitee.com/zhucheer/orange/internal"
	"gitee.com/zhucheer/orange/logger"
	"github.com/zhuCheer/pool"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"sync"
	"time"
)

var mysqlConn *MysqlDB

type MysqlDB struct {
	dsn      string
	connPool map[string]pool.Pool
	count    int
	lock     sync.Mutex
}

// NewMysql 初始化mysql连接
func NewMysql() DataBase {
	if mysqlConn != nil {
		return mysqlConn
	}

	mysqlConn = &MysqlDB{
		connPool: make(map[string]pool.Pool, 0),
	}
	return mysqlConn
}

// 注册所有已配置的mysql
func (my *MysqlDB) RegisterAll() {
	databaseConfig := cfg.Config.GetMap("database.mysql")

	my.count = len(databaseConfig)
	for dd := range databaseConfig {
		my.Register(dd)
	}
}

// NewMysql 注册一个mysql配置
func (my *MysqlDB) Register(name string) {
	addr := cfg.Config.GetString("database.mysql." + name + ".addr")
	username := cfg.Config.GetString("database.mysql." + name + ".username")
	password := cfg.Config.GetString("database.mysql." + name + ".password")
	dbname := cfg.Config.GetString("database.mysql." + name + ".dbname")

	initCap := getDBIntConfig("mysql", name, "initCap")
	maxCap := getDBIntConfig("mysql", name, "maxCap")
	idleTimeout := getDBIntConfig("mysql", name, "idleTimeout")
	isDebug := getBoolConfig("mysql", name, "debug")

	if initCap == 0 || maxCap == 0 || idleTimeout == 0 {
		logger.Error("database config is error initCap,maxCap,idleTimeout should be gt 0")
		return
	}

	dsnPath := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		username, password, addr, dbname)
	my.dsn = dsnPath

	// connMysql 建立连接
	connMysql := func() (interface{}, error) {
		db, err := gorm.Open(mysql.Open(dsnPath), &gorm.Config{})
		if isDebug {
			db = db.Debug()
		}
		return db, err
	}

	// closeMysql 关闭连接
	closeMysql := func(v interface{}) error {
		db, _ := v.(*gorm.DB).DB()
		return db.Close()
	}

	// pingMysql 检测连接连通性
	pingMysql := func(v interface{}) error {
		db, _ := v.(*gorm.DB).DB()
		return db.Ping()
	}

	//创建一个连接池： 初始化5，最大连接30
	p, err := pool.NewChannelPool(&pool.Config{
		InitialCap: initCap,
		MaxCap:     maxCap,
		Factory:    connMysql,
		Close:      closeMysql,
		Ping:       pingMysql,
		//连接最大空闲时间，超过该时间的连接 将会关闭，可避免空闲时连接EOF，自动失效的问题
		IdleTimeout: time.Duration(idleTimeout) * time.Second,
	})
	if err != nil {
		logger.Error("register mysql conn [%s] error:%v", name, err)
		return
	}
	my.insertPool(name, p)
}

// insertPool 将连接池插入map,支持多个不同mysql链接
func (my *MysqlDB) insertPool(name string, p pool.Pool) {
	if my.connPool == nil {
		my.connPool = make(map[string]pool.Pool, 0)
	}

	my.lock.Lock()
	defer my.lock.Unlock()
	my.connPool[name] = p

	hideDsn := hideTcpDsnPasswordLog(my.dsn)
	internal.ConsoleLog(fmt.Sprintf("create Mysql pool [%s](%v) success", name, hideDsn))
}

// getDB 从连接池获取一个连接
func (my *MysqlDB) getDB(name string) (conn interface{}, put func(), err error) {
	put = func() {}

	if _, ok := my.connPool[name]; !ok {
		return nil, put, errors.New("no mysql connect")
	}

	conn, err = my.connPool[name].Get()
	if err != nil {
		return nil, put, errors.New(fmt.Sprintf("mysql get connect err:%v", err))
	}

	put = func() {
		my.connPool[name].Put(conn)
	}

	return conn, put, nil
}

// putDB 将连接放回连接池
func (my *MysqlDB) putDB(name string, db interface{}) (err error) {
	if _, ok := my.connPool[name]; !ok {
		return errors.New("no mysql connect")
	}
	err = my.connPool[name].Put(db)

	return
}

//  GetMysql 获取一个mysql db连接
func GetMysql(name string) (db *gorm.DB, put func(), err error) {
	put = func() {}
	if mysqlConn == nil {
		return nil, put, errors.New("db connect is nil")
	}

	conn, put, err := mysqlConn.getDB(name)
	if err != nil {
		return nil, put, err
	}
	db = conn.(*gorm.DB)
	return db, put, nil
}
