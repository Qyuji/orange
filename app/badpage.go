package app

import "net/http"

// HttpNotFound replies to the request with an HTTP 404 not found error.
func HttpNotFound(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "404 page not found", http.StatusNotFound)
}

// HttpForbidden replies to the request with an HTTP 403 Forbidden error.
func HttpForbidden(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "403 forbidden", http.StatusForbidden)
}
