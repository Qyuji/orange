package app

import (
	"bytes"
	"context"
	"fmt"
	"gitee.com/zhucheer/orange/cfg"
	"gitee.com/zhucheer/orange/httpclient"
	"gitee.com/zhucheer/orange/logger"
	. "github.com/smartystreets/goconvey/convey"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"sync"
	"testing"
	"time"
)

func init() {
	cfg.StartConfig()
	logger.NewLogger()
}

type Route struct {
}

func (r *Route) ServeMux() {

}

func (r *Route) JobDispatch() {

}

func (r *Route) Register() {

}

func TestPprof(t *testing.T) {
	Convey("TestPprof", t, func() {
		httpApiMux := http.NewServeMux()
		httpServer := httptest.NewServer(httpApiMux)
		defer httpServer.Close()
		runPprof(httpApiMux)
		resp, _ := http.Get(httpServer.URL + "/debug/pprof/")
		So(resp.StatusCode, ShouldEqual, 200)
	})
}

func TestHttpBadPage(t *testing.T) {

	Convey("HttpNotFound", t, func() {
		httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			HttpNotFound(w, r)
		}))
		defer httpServer.Close()
		resp, _ := http.Get(httpServer.URL)
		content, _ := ioutil.ReadAll(resp.Body)
		So(resp.StatusCode, ShouldEqual, 404)
		So(strings.Replace(string(content), "\n", "", -1), ShouldEqual, "404 page not found")
	})

	Convey("HttpForbidden", t, func() {
		httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			HttpForbidden(w, r)
		}))
		defer httpServer.Close()
		resp, _ := http.Get(httpServer.URL)
		content, _ := ioutil.ReadAll(resp.Body)
		So(resp.StatusCode, ShouldEqual, 403)
		So(strings.Replace(string(content), "\n", "", -1), ShouldEqual, "403 forbidden")
	})
}

func TestNewCtx(t *testing.T) {
	fileName := "test.txt"
	createFile(fileName)
	Convey("TestNewCtx", t, func() {
		gp := NewRouter("")
		gp.GET("/", func(ctx *Context) error {
			ctx.GetMs()
			ctx.Header().Get("Content-Type")
			ctx.Session().Set("skey", "123")
			ctx.RoutePath()
			return ctx.ToString("123")
		})
		gp.GET("/json", func(ctx *Context) error {
			req := struct {
				Id  string `json:"id"`
				Num int    `json:"num,omitempty"`
			}{}
			ctx.ShouldBind(&req)
			ctx.SetCtxParam("key", "value")
			ctx.GetCtxParam("key")
			return ctx.ToJson(req)
		})
		gp.GET("/query/:id", func(ctx *Context) error {
			id := ctx.GetPathParamToInt("id")
			ctx.AddDelayAfterDo(func(ctx *Context) {
				fmt.Println("delay do")
			}, time.Millisecond)
			ctx.Context()
			ctx.Request()
			ctx.ResponseHeader()
			return ctx.ToJson(id)
		})
		gp.POST("/req", func(ctx *Context) error {
			req := struct {
				Id     string    `json:"id"`
				Num    int       `json:"num"`
				Arr    []string  `json:"arr"`
				ArrInt []int     `json:"arrInt"`
				BoolV  bool      `json:"boolV"`
				BoolV2 bool      `json:"boolV2"`
				BoolV3 bool      `json:"boolV3"`
				Int1   int       `json:"int1"`
				Int2   uint32    `json:"int2"`
				Float1 float64   `json:"float1"`
				TimeV  time.Time `json:"timeV"`
				TimeV2 time.Time `json:"timeV2"`
				TimeV3 time.Time `json:"timeV3"`
				Obj    struct {
					Data string `json:"data"`
					Sint int    `json:"sint"`
				} `json:"obj"`
			}{}
			ctx.ShouldBind(&req)

			fmt.Println(req.Int1, "-----------")
			return ctx.ToJson(req)
		})
		gp.GET("/appdefer", func(ctx *Context) error {
			appDeferDo(ctx.Context())
			return nil
		})
		gp.GET("/file", func(ctx *Context) error {
			return ctx.ToFile(fileName, "file")
		})
		gp.GET("/img", func(ctx *Context) error {
			return ctx.ToImage(fileName)
		})
		gp.GET("/html", func(ctx *Context) error {
			ctx.AddIncludeTmpl("include")
			ctx.ShareAssign("share", "shareValue")
			return ctx.ViewHtml("test", map[string]interface{}{"id": "123"})
		})
		gp.GET("/text", func(ctx *Context) error {
			ctx.AddIncludeTmpl("include")
			ctx.ShareAssign("share", "shareValue")
			return ctx.ViewText("test", map[string]interface{}{"id": "123"})
		})
		gp.GET("/redirect", func(ctx *Context) error {
			return ctx.Redirect("http://gitee.com")
		})
		gp.POST("/upload", func(ctx *Context) error {
			ctx.AppUpload("file")
			ctx.AppUploadToData("file")
			return ctx.ResponseWrite([]byte("123"))
		})
		httpServer := httptest.NewServer(http.HandlerFunc(routerPatten))
		defer httpServer.Close()
		resp, _ := http.Get(httpServer.URL)
		content, _ := ioutil.ReadAll(resp.Body)
		So(string(content), ShouldEqual, "123")

		resp, _ = http.Get(httpServer.URL + "/json?id=123")
		content, _ = ioutil.ReadAll(resp.Body)
		So(string(content), ShouldEqual, `{"id":"123"}`)

		resp, _ = http.Get(httpServer.URL + "/query/100")
		content, _ = ioutil.ReadAll(resp.Body)
		So(string(content), ShouldEqual, `100`)

		httpBody := bytes.NewBuffer([]byte(`{"id":"123","arr":["1","2"],"obj":{"data":"123","sint":1},"arrInt":[1,2,3],"boolV":true,"int1":1,"int2":2}`))
		http.Post(httpServer.URL+"/req", "application/json", httpBody)

		formBody := make(url.Values)
		formBody["id"] = []string{"123"}
		formBody["boolV"] = []string{"1"}
		formBody["int1"] = []string{"1"}
		formBody["int2"] = []string{"1"}
		formBody["arr"] = []string{"1", "2", "3"}
		formBody["float1"] = []string{"1.0"}
		formBody["timeV"] = []string{"2020-10-10 00:00:00"}
		formBody["timeV2"] = []string{"2020-10-10"}
		formBody["timeV3"] = []string{"2019-03-18T10:58:37+08:00"}
		formBody["BoolV2"] = []string{"off"}
		formBody["BoolV3"] = []string{"true"}
		http.PostForm(httpServer.URL+"/req", formBody)

		http.Get(httpServer.URL + "/404")
		http.Get(httpServer.URL + "/file")
		http.Get(httpServer.URL + "/img")
		http.Get(httpServer.URL + "/html")
		http.Get(httpServer.URL + "/text")
		http.Get(httpServer.URL + "/redirect")
		httpclient.NewClient().WithFile("file", fileName).RunPost(httpServer.URL + "/upload")

	})
	time.Sleep(time.Second)
}

func TestRouterAll(t *testing.T) {
	Convey("TestNewCtx", t, func() {
		gp := NewRouter("")
		api := gp.GroupRouter("/api", NewTestMiddleware())
		{
			api.GET("/get/*", func(ctx *Context) error {
				return ctx.ToString("1")
			})
			api.PUT("/put", func(ctx *Context) error {
				return ctx.ToString("1")
			})
			api.POST("/post", func(ctx *Context) error {
				return ctx.ToString("1")
			})
			api.DELETE("/delete", func(ctx *Context) error {
				return ctx.ToString("1")
			})
			api.ALL("/all", func(ctx *Context) error {
				return ctx.ToString("1")
			})

			api.ASSETS("/assets", "./", AutoIndex)
			api.ASSETS("/assets2", "./", DirDeny)
		}

	})
}

func TestAppStart(t *testing.T) {
	client := &Route{}

	go AppStart(client)

}

func TestConsoleStart(t *testing.T) {

	Convey("TestConsoleStart", t, func() {
		route := &Route{}
		ConsoleStart(route)
		JobRun("job1", func(ctx *context.Context) error {
			time.Sleep(1 * time.Second)
			return nil
		}, 3)
		runJobDispatch("")
		runJobDispatch("111")
		//runJobDispatch("job1")
		time.Sleep(2 * time.Second)

	})
}

func TestNewRouter(t *testing.T) {
	Convey("TestNewRouter", t, func() {
		routeGp := NewRouter("")
		handlerRun := func(ctx *Context) error {
			return ctx.ToJson(map[string]interface{}{"data": "orange framework"})
		}
		routeGp.GET("/hello", handlerRun)
		routeGp.GET("/user/:id", handlerRun)
		routeGp.GET("/user/pps", handlerRun)
		routeGp.GET("/user/ppst/*", handlerRun)
		if *routers.maxParam != 1 {
			t.Error("router add error max param")
		}
		deepLen := bfsFindTree(routers.tree)

		So(deepLen, ShouldEqual, 6)
	})

}

func TestNewRouter2(t *testing.T) {
	routeGp := NewRouter("")
	routeGp.GET("/hello", getHandleRun("hello"))
	routeGp.POST("/hello", getHandleRun("post-hello"))
	routeGp.POST("/user/profile", getHandleRun("user/profile"))
	routeGp.GET("/user/:id", getHandleRun("user/:id"))
	routeGp.GET("/list/item", getHandleRun("list/item"))
	routeGp.GET("/list/item/:page/:size", getHandleRun("list/item/:page/:size"))
	routeGp.GET("/list/*", getHandleRun("list/*"))
	routeGp.GET("car/ppt", getHandleRun("car/ppt"))
	routeGp.GET("find/:ip/info", getHandleRun("ip info"))
	httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		routerPatten(w, r)
	}))
	defer httpServer.Close()

	client := httpclient.NewClient()
	resp, _ := client.RunGet(httpServer.URL + "/hello")
	if resp.String() != "hello" {
		t.Error("route find error #1")
	}

	resp, _ = client.RunPost(httpServer.URL + "/hello")
	if resp.String() != "post-hello" {
		t.Error("route find error #2")
	}

	resp, _ = client.RunPost(httpServer.URL + "/user/xiaoming")
	if resp.String() != "Method Not Allowed" {
		t.Error("route find error #3")
	}

	resp, _ = client.RunGet(httpServer.URL + "/user/xiaoming")
	if resp.String() != "user/:id" {
		t.Error("route find error #4")
	}

	resp, _ = client.RunGet(httpServer.URL + "/list/item/15/82")
	if resp.String() != "list/item/:page/:size" {
		t.Error("route find error #5")
	}

	resp, _ = client.RunGet(httpServer.URL + "/list/itemcc")
	if resp.String() != "list/*" {
		t.Error("route find error #6")
	}

	resp, _ = client.RunGet(httpServer.URL + "/car/ppt")
	if resp.String() != "car/ppt" {
		t.Error("route find error #7")
	}

	resp, _ = client.RunGet(httpServer.URL + "/find/192.168.1.1/info")
	if resp.String() != "ip info" {
		t.Error("route find error #8")
	}
}

func BenchmarkRouter_Find(b *testing.B) {
	routeGp := NewRouter("")
	routeGp.GET("/", getHandleRun("hello"))
	routeGp.GET("/user/list", getHandleRun("hello"))
	routeGp.GET("/user/profile", getHandleRun("hello"))
	routeGp.GET("/user/setavatar", getHandleRun("hello"))
	routeGp.GET("/home", getHandleRun("hello"))
	routeGp.DELETE("/home/list/remove", getHandleRun("hello"))
	routeGp.POST("/home/insert/item", getHandleRun("hello"))
	routeGp.GET("/book/info/:id", getHandleRun("hello"))
	routeGp.GET("/book/list/:page/:size", getHandleRun("hello"))
	routeGp.GET("/uinfo/find", getHandleRun("hello"))
	routeGp.POST("/uinfo/copy", getHandleRun("hello"))
	routeGp.GET("/getBoy", getHandleRun("hello"))
	routeGp.GET("/getSign", getHandleRun("hello"))
	routeGp.GET("/auth/token", getHandleRun("hello"))
	routeGp.GET("/auth/refreshToken", getHandleRun("hello"))
	routeGp.GET("/auth/:name/getNewToken", getHandleRun("hello"))
	routeGp.GET("/api/v3/getUserList", getHandleRun("hello"))
	routeGp.PUT("/api/v3/insertItem", getHandleRun("hello"))
	routeGp.GET("/nslookup/:ip", getHandleRun("hello"))
	routeGp.GET("/nslookup/:ip/add", getHandleRun("hello"))
	routeGp.DELETE("/nslookup/:ip/remove", getHandleRun("hello"))

	routeApi := []string{"/user/setavatar", "/nslookup/192.168.1.1/add", "/api/v3/insertItem", "/uinfo/copy"}
	routeRand := routeApi[rand.Intn(4)]

	httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		routerPatten(w, r)
	}))
	defer httpServer.Close()

	client := httpclient.NewClient()
	for i := 0; i < b.N; i++ {
		client.RunGet(httpServer.URL + routeRand)
	}
}

// bfsFindTree 遍历路由树
func bfsFindTree(n *node) (deepLen int) {
	var queue []*node
	queue = append(queue, n)
	for len(queue) > 0 {
		queueLen := len(queue)
		fmt.Println("----------------node-deep----------------")
		deepLen++
		for i := queueLen; i > 0; i-- {
			cn := queue[0]
			parentPrefix := ""
			if cn.parent != nil {
				parentPrefix = cn.parent.prefix
			}
			fmt.Println("kind:", cn.kind, ",lable:", string(cn.label), ",prefix:", cn.prefix, ",ppath:", cn.ppath, ",pnames:", cn.pnames, ",parent->", parentPrefix)
			if len(cn.children) > 0 {
				queue = append(queue, cn.children...)
			}
			queue = queue[1:]
		}
	}
	return
}

func getHandleRun(name string) HandlerFunc {

	return func(ctx *Context) error {
		return ctx.ToString(name)
	}

}

func TestJob_AppendJob(t *testing.T) {
	if len(jobHandler.handlerMap) == 0 {
		t.Error("TestJob_AppendJob err")
	}

	go func() {
		jobHandler.AppendJob("test", JobHandler{
			func(ctx *context.Context) error {
				return nil
			}, 1,
		})
	}()
	go func() {
		jobHandler.AppendJob("test1", JobHandler{
			func(ctx *context.Context) error {
				return nil
			}, 1,
		})
	}()

	go func() {
		jobHandler.AppendJob("test2", JobHandler{
			func(ctx *context.Context) error {
				return nil
			}, 2,
		})
	}()
	time.Sleep(time.Second)
	if len(jobHandler.handlerMap) == 0 {
		t.Error("TestJob_AppendJob2 err")
	}
}

func TestJob_GetJob(t *testing.T) {

	_, err := jobHandler.GetJob("orange")
	if err != nil {
		t.Error("TestJob_GetJob err")
	}
}

func TestAppDefer(t *testing.T) {
	AppDefer(func() {})
	AppDefer(func() {}, func() {})

	if len(exitWaitHandler.deferFuns) != 3 {
		t.Error("AppDefer func have an error #1")
	}

	wg := sync.WaitGroup{}

	wg.Add(2)
	go func() {
		defer wg.Done()
		AppDefer(func() {})
	}()
	go func() {
		defer wg.Done()
		AppDefer(func() {}, func() {})
	}()
	wg.Wait()

	if len(exitWaitHandler.deferFuns) != 6 {
		t.Error("AppDefer func have an error #2")
	}

}

func TestListenStop(t *testing.T) {
	appSig1 := make(chan StopSignal)
	ListenStop(appSig1)

	appSig2 := make(chan StopSignal, 1)
	ListenStop(appSig2)

	appSig3 := make(chan StopSignal, 2)
	ListenStop(appSig3)

	if len(exitWaitHandler.stopSignList) != 3 {
		t.Error("TestListenStop have an error #1")
	}
	sendAppStop()

	go func() {
		select {
		case <-appSig1:
			fmt.Println("app stop appSig1")

		}
	}()

	go func() {
		select {
		case <-appSig2:
			fmt.Println("app stop appSig2")

		}
	}()

	go func() {
		select {
		case <-appSig3:
			fmt.Println("app stop appSig3")

		}
	}()

	time.Sleep(time.Second)
}

func createFile(fileName string) {
	f, err := os.Create(fileName)
	if err != nil {
		fmt.Println(err)
	}
	f.Write([]byte("123"))
	defer f.Close()
}

func removeFile(fileName string) {
	os.Remove(fileName)
}

type testMiddleware struct {
}

func NewTestMiddleware() *testMiddleware {
	return &testMiddleware{}
}

// Func implements Middleware interface.
func (w testMiddleware) Func() MiddlewareFunc {
	return func(next HandlerFunc) HandlerFunc {
		return func(c *Context) error {

			return next(c)
		}
	}
}
