package app

import (
	"gitee.com/zhucheer/orange/cfg"
	"gitee.com/zhucheer/orange/prome"
)

// PromeIncServiceErr 服务异常报错写入Prometheus
func (c *Context) PromeIncServiceErr(serviceName string) {
	if c == nil || cfg.GetBool("prome.open", false) == false {
		return
	}
	promeHandler := prome.PromeHandler()

	promeHandler.SrvErrCnt.WithLabelValues(c.Request().Method, serviceName, c.RoutePath()).Inc()
}
