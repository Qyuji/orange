package app

import (
	"context"
	"fmt"
	"gitee.com/zhucheer/orange/cfg"
	"gitee.com/zhucheer/orange/grpc"
	"gitee.com/zhucheer/orange/internal"
	"gitee.com/zhucheer/orange/logger"
	"gitee.com/zhucheer/orange/prome"
	"gitee.com/zhucheer/orange/utils"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

// HttpSrv http应用注册接口
type HttpSrv interface {
	ServeMux()
	Register()
}

type FileServerType int

const (
	AutoIndex = FileServerType(iota)
	DirDeny

	srvTypeHttp = "http"
	srvTypeGrpc = "grpc"
	methodGRPC  = "GRPC"
	TraceName   = "X-OR-TRACE-ID"
)

// Error handlers
var (
	NotFoundHandler = func(c *Context) error {
		c.responseStatus = http.StatusNotFound
		c.responseBody.Write([]byte("Not Found"))
		c.response.Header().Set("Content-Type", "text/plain; charset=utf-8")
		c.response.Header().Set("X-Content-Type-Options", "nosniff")
		return nil
	}
	MethodNotAllowedHandler = func(c *Context) error {
		c.responseStatus = http.StatusMethodNotAllowed
		c.responseBody.Write([]byte("Method Not Allowed"))
		c.response.Header().Set("Content-Type", "text/plain; charset=utf-8")
		c.response.Header().Set("X-Content-Type-Options", "nosniff")
		return nil
	}
)

type OrangeServer struct {
	inShutdown   int32
	httpSrv      *http.Server
	grpcListener *net.TCPListener
	tcpListener  *net.TCPListener
	mutex        sync.Mutex
}

type appTcpKeepAliveListener struct {
	*net.TCPListener
}

func (ln appTcpKeepAliveListener) Accept() (net.Conn, error) {
	tc, err := ln.AcceptTCP()
	if err != nil {
		return nil, err
	}
	tc.SetKeepAlive(true)
	tc.SetKeepAlivePeriod(time.Minute)
	return tc, nil
}

func NewSrv(httpSrv *http.Server) *OrangeServer {
	return &OrangeServer{
		httpSrv: httpSrv,
	}
}

func (app *OrangeServer) AppListenAndServe() error {
	if app.shuttingDown() {
		return http.ErrServerClosed
	}
	addr := app.httpSrv.Addr
	if addr == "" {
		addr = ":http"
	}
	graceTag := cfg.GetBoolFlag("grace")
	var ln net.Listener
	var err error
	if graceTag {
		f := os.NewFile(3, "")
		ln, err = net.FileListener(f)
	} else {
		ln, err = net.Listen("tcp", addr)
	}
	if err != nil {
		panic(fmt.Sprintf("tcp lister error:%v", err))
	}

	tcpListener, ok := ln.(*net.TCPListener)
	if !ok {
		panic("orange server listener is not tcp listener")
	}
	app.tcpListener = tcpListener
	if err != nil {
		return err
	}
	app.writePidToFile(os.Getpid())
	logger.Info("orange http server start bind:%v, pid:%d", addr, os.Getpid())

	if cfg.Config.GetBool("app.disableKeepAlives") == true {
		app.httpSrv.SetKeepAlivesEnabled(false)
	} else {
		app.httpSrv.SetKeepAlivesEnabled(true)
		app.httpSrv.IdleTimeout = 90 * time.Second
	}
	return app.httpSrv.Serve(tcpListener)
}

func (app *OrangeServer) GetListener() *net.TCPListener {
	return app.tcpListener
}

func (app *OrangeServer) ShutdownDo(ctx context.Context) error {
	atomic.StoreInt32(&app.inShutdown, 1)
	app.clearPidToFile()
	// 关闭grpc客户端连接
	grpc.CloseAll()
	app.GrpcShutDown()
	return app.httpSrv.Shutdown(ctx)
}

// writePidToFile 写入pid到文件
func (app *OrangeServer) writePidToFile(pid int) {
	app.mutex.Lock()
	defer app.mutex.Unlock()
	pidFile := cfg.GetString("app.pidfile", cfg.ConfigDef.GetString("app.pidfile"))
	if pidFile == "" {
		return
	}

	pidFile = strings.Replace(pidFile, "/", utils.DirDot(), -1)
	if pidFile == "" {
		pidFile = "." + utils.DirDot() + "storage" + utils.DirDot() + "orange.pid"
	}
	if pidFile[0:2] == "."+utils.DirDot() {
		pidFile = filepath.Dir(os.Args[0]) + utils.DirDot() + pidFile[2:]
	}
	err := utils.WriteFile(pidFile, strconv.Itoa(pid))
	if err != nil {
		logger.Error("pidfile write pid:%d error:%v", pid, err)
	}
}

func (app *OrangeServer) clearPidToFile() {
	app.mutex.Lock()
	defer app.mutex.Unlock()
	pidFile := cfg.Config.GetString("app.pidfile")
	if pidFile == "" {
		return
	}

	pidFile = strings.Replace(pidFile, "/", utils.DirDot(), -1)
	if pidFile == "" {
		pidFile = "." + utils.DirDot() + "storage" + utils.DirDot() + "orange.pid"
	}
	if pidFile[0:2] == "."+utils.DirDot() {
		pidFile = filepath.Dir(os.Args[0]) + utils.DirDot() + pidFile[2:]
	}

	err := utils.RemoveFile(pidFile)
	if err != nil {
		logger.Error("pidfile clear error:%v", err)
	}

}

func (app *OrangeServer) shuttingDown() bool {
	return atomic.LoadInt32(&app.inShutdown) != 0
}

// startHttpSrv 启动http服务
func startHttpSrv(appSrv HttpSrv) {
	// http请求组件
	initRequest()

	// 注册session
	sessionInit()
	// 注册路由
	appSrv.ServeMux()

	appAddr := cfg.GetString("app.httpAddr", cfg.ConfigDef.GetString("app.httpAddr"))
	appPort := cfg.GetInt("app.httpPort", cfg.ConfigDef.GetInt("app.httpPort"))
	pprofOpen := cfg.GetBool("app.pprofOpen", cfg.ConfigDef.GetBool("app.pprofOpen"))
	flagPort := cfg.GetIntFlag("port")
	if flagPort > 0 {
		appPort = flagPort
	}

	bindAddr := fmt.Sprintf("%s:%d", appAddr, appPort)
	httpApiMux := http.NewServeMux()
	internal.FaviconFunc(httpApiMux)
	if pprofOpen == true {
		runPprof(httpApiMux)
	}

	httpApiMux.HandleFunc("/", routerPatten)

	appName := cfg.GetString("app.name", cfg.ConfigDef.GetString("app.name"))
	httpApiMux.Handle(prome.NewPrometheus(appName).MetricsPath, prome.PrometheusHttpHandler())

	// 用户自定义原生http Handler路由绑定
	for _, item := range httpHandlerList {
		if item.patten != "" && item.patten != "/" {
			httpApiMux.Handle(item.patten, item.handler)
			internal.ConsoleLog("[CustomHttpHandle] " + item.patten)
		}
	}

	// 静态资源路由绑定
	for _, item := range assetsHandlerList {
		if item.patten != "" && item.patten != "/" {
			httpApiMux.Handle(item.patten, item.handler)
			internal.ConsoleLog("[AssetsHttpHandle] " + item.patten)
		}
	}
	server := &http.Server{
		Addr:    bindAddr,
		Handler: httpApiMux,
	}
	httpSrv := NewSrv(server)

	internal.ConsoleLog(fmt.Sprintf("start http server bind:%v, pid:%d", bindAddr, os.Getpid()))
	go func() {
		err := httpSrv.AppListenAndServe()
		fmt.Println(fmt.Sprintf("[ORANGE] \033[0;33m http server shutdown %v \033[0m ", err))
		logger.Critical("http server shutdown %v", err)
	}()

	// 对http服务方法进行grpc服务复刻
	go func() {
		if err := httpSrv.GrpcListenAndStart(); err != nil {
			if err == errGrpcNotOpen {
				fmt.Println(fmt.Sprintf("[ORANGE] \033[0;33m %v \033[0m ", err))
			} else {
				logger.Critical("grpc server shutdown %v", err)
			}
		}
	}()
	listenShutDownSign(context.Background(), httpSrv)

	fmt.Println(fmt.Sprintf("[ORANGE] \033[0;33m app server shutdown pid %d \033[0m ", os.Getpid()))
	logger.Critical("app server shutdown pid %d", os.Getpid())
	time.Sleep(1 * time.Second)
	appCloseDo()
}

// noDirListingHandler 禁止显示目录列表
func noDirListingHandler(h http.Handler, pattenPrefix, basePath string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		relationPath := strings.TrimLeft(r.URL.Path, pattenPrefix)
		indexFilePath := strings.TrimRight(basePath, "/") + "/" + relationPath + "index.html"

		exists, _ := utils.FileExists(indexFilePath)
		if strings.HasSuffix(r.URL.Path, "/") && exists == false {
			HttpForbidden(w, r)
			return
		}
		h.ServeHTTP(w, r)
	})
}

// appCloseDo 应用关闭前最后执行
// 安全关闭业务相关handler
func appCloseDo() {
	logger.LogCloseAll()
}
