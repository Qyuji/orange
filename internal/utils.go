package internal

import (
	"fmt"
	"gitee.com/zhucheer/orange/cfg"
	"gitee.com/zhucheer/orange/encrypt"
	"net/http"
)

// ConsoleLog 输出系统日志
func ConsoleLog(log string) {
	fmt.Println(fmt.Sprintf("\033[0;33m%v \033[0m", "[ORANGE] "+log))
}

// ConsoleRouter 输出已注册路由方法到控制台
func ConsoleRouter(method, patten string) {
	spaceLen := 7 - len(method)
	space := "       "
	space = space[:spaceLen]
	fmt.Println(fmt.Sprintf("[ORANGE] \033[0;33m %s\033[0m%s %s", method, space, patten))
}

// ConsoleColorMsg 输出指定颜色日志
func ConsoleColorMsg(method string, colorNum int) {
	fmt.Println(fmt.Sprintf("[ORANGE] \033[0;%dm %s\033[0m\t ", colorNum, method))
}

// ConsoleWelCome 输出已注册路由方法到控制台
func ConsoleWelCome() {
	console := `
//////////////////////////////////////////
  _____                               
 / ___ \                              
| |   | | ____ ____ ____   ____  ____ 
| |   | |/ ___) _  |  _ \ / _  |/ _  )
| |___| | |  ( ( | | | | ( ( | ( (/ / 
 \_____/|_|   \_||_|_| |_|\_|| |\____)
                         (_____|  
///////////////////////////////////////////
///////     Fast Api Web Framework     ////
/////// gitee.com/zhucheer/orange v0.5 ////
///////////////////////////////////////////

`
	fmt.Println(fmt.Sprintf("\033[0;33m %v \033[0m", console))
}

// FaviconFunc 渲染favicon
func FaviconFunc(httpSrv *http.ServeMux) {
	if hideIcon := cfg.GetBool("app.hideIcon", false); hideIcon {
		return
	}

	httpSrv.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "image/x-icon")
		w.Write(GetFaviconByte())
	})
}

// GetAppKey 获取应用密钥
func GetAppKey() string {
	defaultKey := "orange is fast frame work"
	appKey := cfg.Config.GetString("app.key")
	if appKey == "" || len(appKey) < 8 {
		appKey = defaultKey
	}
	appKeyMd5 := encrypt.Md5ToLower(appKey)
	return appKeyMd5
}
