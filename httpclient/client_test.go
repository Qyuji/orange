package httpclient

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestNewClient(t *testing.T) {
	fmt.Println("start test NewClient")
	client := NewClient()
	client.Header("Test", "123")
	if len(client.header) != 2 {
		t.Error("Header func error")
	}

	client.Header("Content-Type", "application/json")
	if client.contentType != "application/json" {
		t.Error("Header func error #2")
	}

	client.ContentType("application/x-www-form-urlencoded;charset=UTF-8")
	if client.contentType != "application/x-www-form-urlencoded;charset=UTF-8" {
		t.Error("ContentType func error #1")
	}

	client.WithBody(`{"aid":1758,"raw":"werwe"}`)
	if string(client.requestBody) != `{"aid":1758,"raw":"werwe"}` {
		t.Error("Params func error #4")
	}
}

func TestRunGet(t *testing.T) {
	httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("Aid") != "10001" {
			t.Error("NewClient.Header func error #1")
		}

		fmt.Fprint(w, "testing client http")
	}))
	defer httpServer.Close()

	client := NewClient().DisableKeepAlives().SkipHttps().
		Header("User-Agent", "Mozilla/5.0").
		Header("Aid", "10001")
	resp, _ := client.RunGet(httpServer.URL)
	if resp.String() != "testing client http" {
		t.Error("RunGet func error #1")
	}

	resp2, _ := client.WithCookie().RunGet(httpServer.URL)
	if resp2.String() != "testing client http" {
		t.Error("RunGet func error #2")
	}
}

func TestRequestTime(t *testing.T) {
	httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(3 * time.Second)
		w.Header().Set("Cookie-Set", "ssid=test11111")
		fmt.Fprint(w, "testing client http 002")
	}))

	client := NewClient().SetTimeout(5).SkipHttps().DisableKeepAlives()

	resp, _ := client.RunGet(httpServer.URL)
	if resp.GetRequestTime().Seconds() < 3 {
		t.Error("GetRequestTime func error #1")
	}
	if resp.String() != "testing client http 002" {
		t.Error("RunGet func error #1")
	}
}

func TestRunPost(t *testing.T) {
	httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		body, _ := ioutil.ReadAll(r.Body)

		if r.Header.Get("Content-Type") == "application/json" {
			if string(body) != `{"aid":1002,"auth":"ss"}` {
				t.Error("RunPost json type func error")
			}
		}

		if r.Header.Get("Content-Type") == "application/x-www-form-urlencoded" {
			if string(body) != `word=你好` {
				t.Error("RunPost WithFormRequest type func error 'word=你好' != ", string(body))
			}
		}

		if r.Header.Get("Content-Type") == "application/text" {
			if string(body) != `123456789` {
				t.Error("RunPost WithBody type func error")
			}
		}

		fmt.Fprint(w, "testing client http 002")
	}))

	params := map[string]interface{}{
		"aid":  1002,
		"auth": "ss",
	}
	_, err := NewClient().SetTimeout(5).ContentType("application/xxx").
		FormParams(params).RunPost(httpServer.URL)
	if err == nil {
		t.Error("RunPost func error #1")
	}

	client := NewClient().SetTimeout(5).WithCookie()
	client.FormParams(params).WithJsonRequest().RunPost(httpServer.URL)

	params = map[string]interface{}{
		"word": "你好",
	}
	client.FormParams(params).WithFormRequest().RunPost(httpServer.URL)

	ret, _ := client.ResetParam().ContentType("application/text").WithBody("123456789").RunPost(httpServer.URL)
	if ret.String() == "" {
		t.Error("RunPost func error #2")
	}
	fmt.Println(ret.GetRequestTime(), client.Error())
}

func TestRunJsonPost(t *testing.T) {
	httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		body, _ := ioutil.ReadAll(r.Body)

		if r.Header.Get("Content-Type") == "application/json" {
			if string(body) != `{"aid":1002,"auth":"ss"}` {
				t.Error("RunPost json type func error")
			}
		}

		fmt.Fprint(w, "testing client http 002")
	}))

	params := struct {
		Aid  int    `json:"aid"`
		Auth string `json:"auth"`
	}{1002, "ss"}

	client := NewClient().SetTimeout(5).WithCookie()
	client.WithJsonBody(params).RunPost(httpServer.URL)

}
