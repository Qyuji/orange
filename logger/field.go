package logger

import (
	"fmt"
	"reflect"
)

type Field struct {
	Key   string
	Type  reflect.Type
	Value interface{}
}

func Reflect(key string, valType reflect.Type, val interface{}) Field {
	return Field{Key: key, Type: valType, Value: val}
}

func Any(key string, value interface{}) Field {
	valType := reflect.TypeOf(value)
	if valType == nil {
		return Reflect(key, valType, fmt.Sprintf("%v", value))
	}

	var sv interface{}
	switch valType.Kind() {
	case
		reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64,
		reflect.Uint,
		reflect.Uint8,
		reflect.Uint16,
		reflect.Uint32,
		reflect.Uint64,
		reflect.Float32,
		reflect.Float64:
		sv = value
	default:
		sv = fmt.Sprintf("%v", value)
	}

	return Reflect(key, valType, sv)
}
