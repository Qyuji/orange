package logger

import (
	"context"
	"fmt"
	"gitee.com/zhucheer/orange/cfg"
	"gitee.com/zhucheer/orange/internal"
	"strings"
)

var logFacede *Logger

// 初始化logger快速方法
func NewLogger() {
	loggerLevel, loggerType, loggerPath, loggerSyncInterval, maxCap := getLoggerConfig()

	if loggerType != "json" && loggerType != "text" {
		loggerType = "text"
	}

	level := INFO
	switch loggerLevel {
	case "DEBUG":
		level = DEBUG
	case "INFO":
		level = INFO
	case "NOTICE":
		level = NOTICE
	case "WARNING":
		level = WARNING
	case "ERROR":
		level = ERROR
	case "CRITICAL":
		level = CRITICAL
	}

	logMsgChan = make(chan loggerFileChan, maxCap)
	logFacede = New(level, loggerType, loggerPath, loggerSyncInterval)
	internal.ConsoleLog(fmt.Sprintf("logger start level[%v] type[%v] syncInterval[%v]",
		LevelString[level], loggerType, loggerSyncInterval))

	// 初始化kafka推送
	initKafkaHandler()
}

func getLoggerConfig() (level, logType, logPath string, interval, maxCap int) {
	if cfg.Config == nil {
		return "INFO", TextType, "", 200, 5000
	}

	level = strings.ToUpper(cfg.GetString("app.logger.level", cfg.ConfigDef.GetString("app.logger.level")))
	logType = cfg.GetString("app.logger.type", cfg.ConfigDef.GetString("app.logger.type"))
	logPath = cfg.GetString("app.logger.path", cfg.ConfigDef.GetString("app.logger.path"))
	interval = cfg.GetInt("app.logger.syncInterval", cfg.ConfigDef.GetInt("app.logger.syncInterval"))
	maxCap = cfg.GetInt("app.logger.maxCap", cfg.ConfigDef.GetInt("app.logger.maxCap"))
	return
}

// LogFile, new log file save.
func LogFile(fileName string) *Logger {
	checkLogNil()
	return logFacede.LogFile(fileName)
}

// WithContext, 注入上下文.
func WithContext(c context.Context) *Logger {
	checkLogNil()
	traceId, _ := c.Value(TraceName).(string)
	hasTrace, _ := logFacede.ctx.Value(TraceName).(string)
	if traceId == "" && hasTrace == "" {
		logFacede.ctx = context.WithValue(c, TraceName, getTraceId(c))
	}
	if traceId != "" {
		logFacede.ctx = context.WithValue(c, TraceName, traceId)
	}
	return logFacede
}

// Debug, record DEBUG message.
func Debug(format string, a ...interface{}) {
	checkLogNil()
	logFacede.Debug(format, a...)
}

// Info, record INFO message.
func Info(format string, a ...interface{}) {
	checkLogNil()
	logFacede.Info(format, a...)
}

// Notice, record INFO message.
func Notice(format string, a ...interface{}) {
	checkLogNil()
	logFacede.Notice(format, a...)
}

// Warning, record WARNING message.
func Warning(format string, a ...interface{}) {
	checkLogNil()
	logFacede.Warning(format, a...)
}

// Error, record ERROR message.
func Error(format string, a ...interface{}) {
	checkLogNil()
	logFacede.Error(format, a...)
}

// Critical, record CRITICAL message.
func Critical(format string, a ...interface{}) {
	checkLogNil()
	logFacede.Critical(format, a...)
}

// Debugw, record DEBUG message  with kv format.
func Debugw(message string, kv ...interface{}) {
	checkLogNil()
	logFacede.Debugw(message, kv...)
}

// Infow, record INFO message  with kv format.
func Infow(message string, kv ...interface{}) {
	checkLogNil()
	logFacede.Infow(message, kv...)
}

// Noticew, record INFO message  with kv format.
func Noticew(message string, kv ...interface{}) {
	checkLogNil()
	logFacede.Noticew(message, kv...)
}

// Warningw, record WARNING message  with kv format.
func Warningw(message string, kv ...interface{}) {
	checkLogNil()
	logFacede.Warningw(message, kv...)
}

// Errorw, record ERROR message  with kv format.
func Errorw(message string, kv ...interface{}) {
	checkLogNil()
	logFacede.Errorw(message, kv...)
}

// Criticalw, record CRITICAL message  with kv format.
func Criticalw(message string, kv ...interface{}) {
	checkLogNil()
	logFacede.Criticalw(message, kv...)
}

func LogCloseAll() {
	if logFacede == nil {
		return
	}
	logFacede.LogDeferClose()

	// 关闭队列
	CloseProducer()
}

// LogIsStart, check is open.
func LogIsStart() bool {
	if logFacede == nil {
		return false
	}
	return true
}

// checkLogNil check log should not empty
func checkLogNil() {
	if logFacede == nil {
		panic("log is nil, init log first")
	}
}
