package logger

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"sync"
	"testing"
	"time"
)

func TestLogCocurrent(t *testing.T) {
	logDir := "./"
	logFileName := "cocurrent.log"
	defer func() {
		os.Remove(logDir + logFileName)
	}()
	logger := New(INFO, TextType, logDir, 40000)
	time.Sleep(500 * time.Millisecond)
	var wg sync.WaitGroup
	wg.Add(3)
	go func() {
		wg.Done()
		logger.LogFile("cocurrent").Info("1test Info 1111")
	}()
	go func() {
		wg.Done()
		logger.LogFile("cocurrent").Info("2test Info 2222")
	}()
	go func() {
		wg.Done()
		logger.LogFile("cocurrent").Info("3test Info 3333")

	}()
	wg.Wait()
	time.Sleep(500 * time.Millisecond)
	logString := logger.LogFile("cocurrent").StreamHandler.LogBuffer.String()

	if strings.Index(logString, "Info 1111") < 0 {
		t.Error("Log currecy is error #1")
	}

	if strings.Index(logString, "Info 2222") < 0 {
		t.Error("Log currecy is error #2")
	}

	if strings.Index(logString, "Info 3333") < 0 {
		t.Error("Log currecy is error #3")
	}

	time.Sleep(4 * time.Second)
}

func TestNewLogger(t *testing.T) {
	NewLogger()

	if LevelString[logFacede.Level] != "INFO" || logFacede.Format.FormatType != "text" || logFacede.SyncInterval != 200 {
		t.Error("getLoggerConfig has error")
	}

}

func TestFuncField2Map(t *testing.T) {
	fields := []Field{
		{Key: "ttt", Value: 122},
		{Key: "", Value: 122},
	}

	outMap := field2Map(fields)
	if len(outMap) != 1 {
		t.Error("New field2Map error #1")
	}
}

func TestFuncJsonStructify(t *testing.T) {
	cc := struct {
		AA string   `json:"aa"`
		BB int      `json:"bb"`
		TT int      `json:"-"`
		ZZ []string `json:"zz"`
		CC []string
	}{
		"22", 33, 55, []string{"1", "2"}, []string{"1", "5"},
	}
	testprt := "test"
	outMap, ok := jsonStructify("123")
	if ok != false {
		t.Error("New jsonStructify error #1")
	}

	outMap, ok = jsonStructify(&testprt)
	if ok != false {
		t.Error("New jsonStructify error #2")
	}

	outMap, ok = jsonStructify(cc)
	if ok != true {
		t.Error("New jsonStructify error #3")
	}
	if _, ok := outMap["TT"]; ok {
		t.Error("New jsonStructify error #4")
	}

	if len(outMap) != 4 {
		t.Error("New jsonStructify error #5")
	}
}

func TestLogNew(t *testing.T) {
	logDir := "./"
	logFileName := "default.log"
	defer func() {
		os.Remove(logDir + logFileName)
	}()
	logger := New(DEBUG, JsonType, "", 0)
	if logger.StreamHandler.LogFilePath != "" {
		t.Error("New logger error #1")
	}

	logger = New(DEBUG, TextType, logDir, 0)
	if logger.StreamHandler.LogFilePath != "./default.log" {
		t.Error("New logger error #2")
	}
	logger.LogDeferClose()
}

func TestLogDefault(t *testing.T) {
	logDir := "./"
	logFileName := "default.log"
	defer func() {
		os.Remove(logDir + logFileName)
	}()

	logger := New(DEBUG, TextType, logDir, 0)
	logger.Debug("test log %s", "xxxx")
	logger.Debugw("test log ", "userkey", "xxxx")
	time.Sleep(time.Second)
	if fileExists(logDir+logFileName) == false {
		t.Error("log write file error")
	}
	logContent, _ := readFileAll(logDir + logFileName)
	if strings.Contains(logContent, `test log xxxx`) == false {
		t.Error("TestLogDefault error #1", logContent)
	}
	if strings.Contains(logContent, `"userkey":"xxxx"`) == false {
		t.Error("TestLogDefault error #2")
	}
	logger.LogDeferClose()
	time.Sleep(time.Second)
}

func TestLogFileNew(t *testing.T) {
	logDir := "./"
	logFileName := "default.log"
	logFileName2 := "test01.log"
	defer func() {
		os.Remove(logDir + logFileName)
		os.Remove(logDir + logFileName2)
	}()
	logger := New(DEBUG, JsonType, logDir, 1000)
	logger.Debug("test log %s", "xxxx")
	var err error
	logger.Errorw("test log ", "err=>", err)

	logger.LogFile("test01").Infow("test2 info log")
	logger.LogFile("test01").Info("test2 info log %s", "xxxxx")

	time.Sleep(2 * time.Second)

	if fileExists(logDir+logFileName) == false {
		t.Error("log write file error")
	}
	if fileExists(logDir+logFileName2) == false {
		t.Error("log write file error")
	}

	logContent, _ := readFileAll(logDir + logFileName2)
	if strings.Contains(logContent, `"log_file":"test01"`) == false {
		t.Error("TestLogFile error #1")
	}

	logger.LogDeferClose()
}

func TestLogFileClose(t *testing.T) {
	logDir := "./"
	logFileName := "default.log"
	logFileName2 := "test01.log"
	defer func() {
		os.Remove(logDir + logFileName)
		os.Remove(logDir + logFileName2)
	}()

	logger := New(DEBUG, JsonType, logDir, 1000)
	logger.Debug("test log %s", "xxxx")
	logger.LogFile("test01").Info("test info log %s", "xxxxx")
	logger.LogFile("test01").Info("test2 info log %s", "xxxxx")

	if len(loggerFiles.LoggerMap) != 2 {
		t.Error("LogFileClose error#1")
	}
	logger.LogDeferClose()
	if len(loggerFiles.LoggerMap) != 0 {
		t.Error("LogFileClose error#2")
	}
}

func TestLogMaxLine(t *testing.T) {
	logger := New(INFO, TextType, "", 300)

	for i := 0; i < 1001; i++ {
		logger.Info("1test Info 1111" + strconv.Itoa(i))
	}
	time.Sleep(1000 * time.Millisecond)
	logString := logger.StreamHandler.LogBuffer.String()
	if logString != "" {
		t.Error("TestLogMaxLine error#1")
	}
}

func TestLogConsole(t *testing.T) {
	logger := New(DEBUG, TextType, "", 1000)
	logger.Debug("test Debug")
	logger.Info("test Info")
	logger.Notice("test Notice")
	logger.Warning("test Warning")
	logger.Error("test Error")
	logger.Critical("test Critical")

	logger.Debugw("test Debug")
	logger.Infow("test Info")
	logger.Noticew("test Notice")
	logger.Warningw("test Warning")
	logger.Errorw("test Error")
	logger.Criticalw("test Critical")

	logger = New(DEBUG, JsonType, "", 1000)
	logger.Debug("test Debug")
	logger.Info("test Info")
	logger.Notice("test Notice")
	logger.Warning("test Warning")
	logger.Error("test Error")
	logger.Critical("test Critical")

	logger.Debugw("test Debug", "err", errors.New("test errmsg"))
	logger.Infow("test Info")
	logger.Noticew("test Notice")
	logger.Warningw("test Warning")
	logger.Errorw("test Error")
	logger.Criticalw("test Critical")
	var err error
	logger.Errorw("test log ", "err=>", err)

	logger.LogFile("test02").Info("test02 info log %s", "xxxxx")
	logger.LogFile("test02").Error("test02 error log %s", "xxxxx")
	time.Sleep(2 * time.Second)
}

func TestMoreLogFile(t *testing.T) {
	logger := New(DEBUG, TextType, "", 1000)

	for i := 1; i < 20; i++ {
		logFileName := fmt.Sprintf("test_%d", i)
		logger.LogFile(logFileName).Info("test info log %s", logFileName)
	}

	time.Sleep(2 * time.Second)
}

func TestLogNil(t *testing.T) {
	logFacede = nil

	defer func() {
		err := recover()
		if err == nil {
			t.Error("checkLogNil func have an err")
		}
	}()

	Debug("test log nil")

}

func TestLogFacede(t *testing.T) {

	logFacede = New(DEBUG, JsonType, "", 1)

	Debug("test Debug")
	Info("test Info")
	Notice("test Notice")
	Warning("test Warning")
	Error("test Error")
	Critical("test Critical")

	logFacede = New(DEBUG, TextType, "", 1)
	Debugw("test Debug", "debugtt", "1111111111")
	Infow("test Info", "info", "1111111111")
	Noticew("test Notice")
	Warningw("test Warning")
	Errorw("test Error")
	Criticalw("test Critical")

	LogFile("tes").Debugw("debug test")
	LogIsStart()

	time.Sleep(1 * time.Second)
}

func BenchmarkDebugw(b *testing.B) {
	logger := New(DEBUG, JsonType, "", 50)
	for i := 0; i < b.N; i++ {
		logger.Debugw("Benchmark", "debugKey", "testvalue")
	}
}

func BenchmarkDebugLog(b *testing.B) {
	logger := New(DEBUG, JsonType, "", 50)
	for i := 0; i < b.N; i++ {
		logger.Debug("Benchmark %v", "teswt")
	}
}

func BenchmarkAny(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Any(fmt.Sprintf("index-%d", i), "test")
	}
}

// 判断所给路径文件/文件夹是否存在
func fileExists(path string) bool {
	_, err := os.Stat(path) //os.Stat获取文件信息
	if err != nil {
		if os.IsExist(err) {
			return true
		}
		return false
	}
	return true
}

func readFileAll(filePth string) (string, error) {
	f, err := os.Open(filePth)
	if err != nil {
		return "", err
	}

	content, err := ioutil.ReadAll(f)

	return string(content), err
}
