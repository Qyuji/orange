package tpl

var indexGo = `package controller

import (
	"context"
	"fmt"
	"gitee.com/zhucheer/orange/app"
	"time"
)

func Welcome(c *app.Context) error {

	return c.ToString(` + "`{{.indexHtml}}`" + `)
}

func AuthCheck(c *app.Context) error {

	return c.ToJson(map[string]interface{}{
		"auth": "auth is ok",
	})
}

func JobShowTime(c *context.Context)error{
	fmt.Println("显示当前时间:",time.Now().String())
	return nil
}`
