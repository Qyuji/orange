package tpl

import (
	"sync"
)

var generatorTmpMap map[string]string
var mutex sync.Mutex

// init 初始化脚手架数据
func init() {
	registerTpl("config", config)
	registerTpl("indexgo", indexGo)
	registerTpl("indexhtml", indexHtml)
	registerTpl("main", mainGo)
	registerTpl("job", jobGo)
	registerTpl("middleware", middleware)
	registerTpl("route", route)
	registerTpl("jobRoute", jobRoute)
	registerTpl("tpldemo", demoHtml)
}

// registerTpl 注册脚手架模板内容
func registerTpl(tplName string, content string) {
	mutex.Lock()
	defer mutex.Unlock()

	if generatorTmpMap == nil {
		generatorTmpMap = make(map[string]string)
	}

	generatorTmpMap[tplName] = content
}

func GetTmp(tmpName string) string {
	if tmpContent, ok := generatorTmpMap[tmpName]; ok {
		return tmpContent
	}
	return ""
}
