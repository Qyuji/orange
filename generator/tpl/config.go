package tpl

var config = `[app]
    name = "orange"
    key = "{{.appkey}}"
    debug = false
    httpAddr = "0.0.0.0"
    httpPort = 8088
    maxBody = 2096157
    csrfVerify = false
    maxWaitSecond = 120
    viewPath = "./storage/views"
    [app.logger]
        level = "INFO"
        type = "text"
        path = ""
        syncInterval = 200
    [app.session]
        isOpen = false
        driver = "cookie"
        timeout = 1800
    [app.upload]
        storage = "./storage/allimg"
        maxSize = 2096157
        ext = ["jpg","jpeg","gif","png"]`
