package tpl

var mainGo = `package main

import (
	"gitee.com/zhucheer/orange/app"
	"{{.projectName}}/route"
)

func main() {

	router := &route.Route{}
	app.AppStart(router)

}`

var jobGo = `package main

import (
	"gitee.com/zhucheer/orange/app"
	"{{.projectName}}/route"
)

func main() {

	router := &route.Route{}
	app.ConsoleStart(app.JobSrv(router))

}`
