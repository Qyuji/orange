package tpl

var route = `package route

import (
	"{{.projectName}}/http/controller"
	"{{.projectName}}/http/middleware"
	"gitee.com/zhucheer/orange/app"
)

type Route struct {
}

func (s *Route) ServeMux() {
	commonGp := app.NewRouter("")
	{
		commonGp.GET("/", controller.Welcome)

		commonGp.GET("/api", func(ctx *app.Context) error {
			return ctx.ToJson(map[string]interface{}{"data": "orange framework"})
		})
	}

	authGp := app.NewRouter("/auth", middleware.NewAuth())
	{
		authGp.ALL("/info", controller.AuthCheck)
	}

}


// Register 服务注册器, 可以对业务服务进行初始化调用
func (s *Route) Register() {

}
`

var jobRoute = `package route

import (
	"{{.projectName}}/http/controller"
	"gitee.com/zhucheer/orange/app"
	"time"
)

func (s *Route) JobDispatch() {
	app.JobRun("showTime",  controller.JobShowTime, 30*time.Second)
}
`
