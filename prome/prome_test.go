package prome

import (
	"testing"
)

func TestNewPrometheus(t *testing.T) {
	p := NewPrometheus("test_go")

	if p == nil {
		t.Error("err1")
	}
	if p.subsystem != "test_go" {
		t.Error("err2")
	}
}

func TestPrometheusHttpHandler(t *testing.T) {
	PrometheusHttpHandler()
}

func TestPromeHandler(t *testing.T) {
	PromeHandler()
}

func TestNewMetric(t *testing.T) {
	testMetric := Metric{
		Name:        "Test",
		Description: "Test desc",
		Type:        CounterVec,
		Args:        []string{"test"},
	}

	NewMetric(&testMetric, "Test")

	testMetric.Type = GaugeVec
	NewMetric(&testMetric, "Test")

	testMetric.Type = Gauge
	NewMetric(&testMetric, "Test")

	testMetric.Type = HistogramVec
	NewMetric(&testMetric, "Test")

	testMetric.Type = Histogram
	NewMetric(&testMetric, "Test")

	testMetric.Type = SummaryVec
	NewMetric(&testMetric, "Test")

	testMetric.Type = Summary
	NewMetric(&testMetric, "Test")
}

func TestPrometheus_PromeMetrics(t *testing.T) {
	p := NewPrometheus("test_go")

	p.RegisterCustomerMetrics("gogogo", "ssss", CounterVec, []string{"a", "b"})
	p.PromeMetrics("gogogo")
}
