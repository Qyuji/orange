module gitee.com/zhucheer/orange

go 1.14

require (
	gitee.com/zhucheer/cfg v0.2.0
	github.com/Shopify/sarama v1.27.2
	github.com/apache/rocketmq-client-go/v2 v2.1.1
	github.com/fogleman/gg v1.3.0
	github.com/go-pg/pg/extra/pgdebug v0.2.0
	github.com/go-pg/pg/v10 v10.10.6
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/google/uuid v1.3.0
	github.com/juju/ratelimit v1.0.1
	github.com/nacos-group/nacos-sdk-go v1.1.1
	github.com/prometheus/client_golang v1.7.1
	github.com/smartystreets/goconvey v1.6.4
	github.com/stretchr/testify v1.7.0
	github.com/zhuCheer/pool v0.2.1
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1
	google.golang.org/grpc v1.27.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gorm.io/driver/clickhouse v0.2.1
	gorm.io/driver/mysql v1.0.5
	gorm.io/driver/postgres v1.1.1
	gorm.io/driver/sqlserver v1.0.9
	gorm.io/gorm v1.21.15
)
