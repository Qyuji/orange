package session

import (
	"errors"
	"net/http"
)

var globalSessions *Manager

type SessionHandler struct {
	handler Store
}

func init() {
	Register("cookie", cookieProvider)
	Register("redis", redisProvider)
}

// NewSession 注册session
func NewSession(driver string, config *ManagerConfig) (err error) {
	if globalSessions != nil {
		return errors.New("session has been init")
	}

	globalSessions, err = NewManager(driver, config)
	return err
}

// SessionStart 启动session
func SessionStart(writer http.ResponseWriter, request *http.Request) (sessHandler Store, err error) {
	checkGlobalSession()

	sessHandler, err = globalSessions.SessionStart(writer, request)
	if sessHandler == nil {
		return
	}
	return
}

func checkGlobalSession() {
	if globalSessions == nil {
		panic("session not init")
	}
}

func (s *SessionHandler) Set(key, value interface{}) error {

	return s.handler.Set(key, value)
}

func (s *SessionHandler) Get(key interface{}) interface{} {
	return s.handler.Get(key)
}
