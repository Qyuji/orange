package session

import (
	"fmt"
	"gitee.com/zhucheer/orange/cfg"
	"gitee.com/zhucheer/orange/database"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

// go test ./ -v -args --config=../project/config/config.toml
func startCfg() {
	cfg.ParseParams()
	cfg.StartConfig()
}

func TestSessByCookie(t *testing.T) {

	var globalSessions *Manager

	httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("test http api")

		config := &ManagerConfig{
			CookieName:      "orange",
			Gclifetime:      1800,
			DisableHTTPOnly: true,
			ProviderConfig:  "{}",
		}

		globalSessions, _ = NewManager("cookie", config)
		sid, _ := globalSessions.sessionID()

		sess, _ := globalSessions.SessionStart(w, r)

		sess.Set("ss", "6666")

		fmt.Println(sess.Get("ss"))

		fmt.Println(sid)

	}))
	defer httpServer.Close()
	httpUrl, _ := url.Parse(httpServer.URL)

	res, err := http.Get(httpUrl.String())
	if err != nil {
		t.Error(err)
	}
	greeting, _ := ioutil.ReadAll(res.Body)
	res.Body.Close()

	fmt.Println(string(greeting))
}

func TestSessByRedis(t *testing.T) {
	startCfg()
	database.NewRedis().RegisterAll()
	var globalSessions *Manager
	httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		config := &ManagerConfig{
			CookieName:      "orange",
			Gclifetime:      1800,
			DisableHTTPOnly: true,
			ProviderConfig:  "{}",
		}

		globalSessions, _ = NewManager("redis", config)
		sid, _ := globalSessions.sessionID()

		sess, _ := globalSessions.SessionStart(w, r)

		sess.Flush()
		sess.Set("ss", "6666")
		sess.Delete("ss")
		fmt.Println(sess.Get("ss"))

		fmt.Println(sid)

	}))
	defer httpServer.Close()
	httpUrl, _ := url.Parse(httpServer.URL)

	res, err := http.Get(httpUrl.String())
	if err != nil {
		t.Error(err)
	}
	greeting, _ := ioutil.ReadAll(res.Body)
	res.Body.Close()

	fmt.Println(string(greeting))
}
