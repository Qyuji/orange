package mailer

import (
	"testing"
	"gitee.com/zhucheer/orange/cfg"
)

func TestNewMailer(t *testing.T){
	// go test smtp_test.go smtp.go init.go -v -args --config=../project/config/config.toml
	cfg.ParseParams()
	cfg.StartConfig()

	defaultMailer, err := NewMailer("default")
	if err != nil{
		t.Errorf("NewMailer have error #1 %v", err)
	}
	if defaultMailer.option.Host == ""{
		t.Error("defaultMailer no Host")
	}

	_,err=NewMailer("mailer01")
	if err.Error() != "not found mailer config by mailer01"{
		t.Error("mailer read have an error")
	}

}

func TestGetSendMailerDefaultPort(t *testing.T) {
	mailer,err:= GetSendMailer(Mailer{
		Host:"smtp.qq.com",
		Port:25,
		UserName:"000@qq.com",
		Password:"000",
		OpenTsl:false,
	})

	err = mailer.To([]string{"zhu2943@qq.com"}).Cc([]string{"zhu.cheer@qq.com"}).SetText().Subject("test abc").Send("hello")

	if err != nil{
		t.Error(err)
	}
}

func TestGetSendMailer(t *testing.T) {
	mailer,err:= GetSendMailer(Mailer{
		Host:"smtp.qq.com",
		Port:465,
		UserName:"000@qq.com",
		Password:"000",
		OpenTsl:true,
	})


	cid, err:=mailer.AddAttachment("./attachment.jpg")

	mailer.To([]string{"zhu.cheer@qq.com"}).From("Orange Test").Cc([]string{"zhu.cheer@qq.com"}).Bcc([]string{"zhu.cheer@qq.com"}).
		SetHtml().Subject("test cc").Send("<b>bcc mailer</b> with an attachment")

	if cid == "" || err != nil{
		t.Error("add attachment is error")
	}

}
