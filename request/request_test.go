package request

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func TestNewInput(t *testing.T) {

	httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		inputHandler := NewInput(r, 2048)

		if inputHandler.Protocol() != "HTTP/1.1" {
			t.Error("Protocol is error")
		}

		if inputHandler.URI() != "/abccd?test=hello" {
			t.Error("URI() is error")
		}

		if inputHandler.URL() != "/abccd" {
			t.Error("URL() is error")
		}

		if inputHandler.Host() != "127.0.0.1" {
			t.Error("Host() is error")
		}

	}))
	defer httpServer.Close()

	path := fmt.Sprintf("/abccd?test=%s", "hello")

	httpUrl, _ := url.Parse(httpServer.URL + path)

	http.Get(httpUrl.String())

}
